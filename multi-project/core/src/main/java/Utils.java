import org.apache.commons.lang3.math.NumberUtils;

public class Utils {

    public static boolean isAllPositiveNumbers(String... str) {
        for (String s : str) {
            if (!NumberUtils.isParsable(s))
                return false;
            if (!(Double.parseDouble(s) > 0))
                return false;
        }
        return true;
    }
}
