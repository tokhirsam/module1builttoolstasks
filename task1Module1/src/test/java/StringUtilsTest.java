import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {
    @Test
    void isPositiveNumber() {
        Assertions.assertFalse(StringUtils.isPositiveNumber("-12"));

        Assertions.assertFalse(StringUtils.isPositiveNumber("just"));

        Assertions.assertFalse(StringUtils.isPositiveNumber("us300"));

        Assertions.assertTrue(StringUtils.isPositiveNumber("2"));

        Assertions.assertTrue(StringUtils.isPositiveNumber("138.2"));

        Assertions.assertTrue(StringUtils.isPositiveNumber("09"));
    }
}
